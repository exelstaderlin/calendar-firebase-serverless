'use strict';

const functions = require('firebase-functions');
const fs = require('fs');


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

// To deploy codes use this : firebase deploy --only functions
// to test API example : https://us-central1-calendar-international.cloudfunctions.net/helloWorld


exports.helloWorld = functions.https.onRequest((request, response) => {
 response.send("Hello from Firebase!");
});


exports.test = functions.https.onRequest((request, response) => {
    const returnObj = {};
    returnObj['queryStrings'] = request.query;
    returnObj['body'] = request.body;

    response.send(returnObj);
});

exports.checkversion = functions.https.onRequest((request, response) => {
    const version = '4';
    response.send(version);
});


exports.indonesia = functions.https.onRequest((request, response) => {
    fs.readFile('calendar_indonesia_2019.json', (err, data) => {
        if (err) throw err;
        let json;
        json = JSON.parse(data);
        console.log(json);
        response.send(json);
    });
});


exports.singapore = functions.https.onRequest((request, response) => {
    fs.readFile('calendar_singapore_2019.json', (err, data) => {
        if (err) throw err;
        let json;
        json = JSON.parse(data);
        console.log(json);
        response.send(json);
    });
});


exports.malaysia = functions.https.onRequest((request, response) => {
    fs.readFile('calendar_malaysia_2019.json', (err, data) => {
        if (err) throw err;
        let json;
        json = JSON.parse(data);
        console.log(json);
        response.send(json);
    });
});

exports.thailand = functions.https.onRequest((request, response) => {
    fs.readFile('calendar_thailand_2019.json', (err, data) => {
        if (err) throw err;
        let json;
        json = JSON.parse(data);
        console.log(json);
        response.send(json);
    });
});

exports.myanmar = functions.https.onRequest((request, response) => {
    fs.readFile('calendar_myanmar_2019.json', (err, data) => {
        if (err) throw err;
        let json;
        json = JSON.parse(data);
        console.log(json);
        response.send(json);
    });
});

exports.vietnam = functions.https.onRequest((request, response) => {
    fs.readFile('calendar_vietnam_2019.json', (err, data) => {
        if (err) throw err;
        let json;
        json = JSON.parse(data);
        console.log(json);
        response.send(json);
    });
});

exports.cambodia = functions.https.onRequest((request, response) => {
    fs.readFile('calendar_cambodia_2019.json', (err, data) => {
        if (err) throw err;
        let json;
        json = JSON.parse(data);
        console.log(json);
        response.send(json);
    });
});

